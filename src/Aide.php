<?php

namespace Drupal\aide;

use Drupal\block\Entity\Block;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Url;
use Drupal\image\Entity\ImageStyle;
use Drupal\node\NodeInterface;
use Drupal\responsive_image\Entity\ResponsiveImageStyle;
use Drupal\user\Entity\User;
use Drupal\user\UserInterface;

/**
 * Provides a class for the Aide utility.
 *
 * This is a utility class intended to be used within the context of hooks.
 *
 * Please remember that dependency injection is always the preferred solution
 * within classes and services.
 */
final class Aide {

  /**
   * Get the current path.
   *
   * This function will return a path alias if available.
   *
   * @return string
   *   The current raw path.
   */
  public static function getCurrentPath() : string {
    static $current_path_stack;
    $current_path_stack ??= \Drupal::service('path.current');
    return \Drupal::service('path_alias.manager')->getAliasByPath($current_path_stack->getPath());
  }

  /**
   * Get the request URI.
   *
   * @return string
   *   The URI of the current request.
   */
  public static function getRequestUri() : string {
    static $request;
    $request ??= \Drupal::request();
    return $request->getRequestUri();
  }

  /**
   * Get the name of the current route.
   *
   * @return string
   *   The current route.
   */
  public static function getCurrentRouteName() : string {
    static $route_match;
    $route_match ??= \Drupal::routeMatch();
    return $route_match->getRouteName();
  }

  /**
   * Extract node from the current route, if any.
   *
   * @return \Drupal\node\NodeInterface|null
   *   The current node extracted from the route, if any.
   */
  public static function getCurrentNode() : ?NodeInterface {
    static $route_match, $node;
    if (!empty($node)) {
      return $node;
    }
    $route_match ??= \Drupal::routeMatch();
    $node ??= $route_match->getParameter('node');
    if ($node instanceof NodeInterface) {
      return $node;
    }
    else {
      return NULL;
    }
  }

  /**
   * Utility function to get the current user.
   *
   * Won't be used much in my opinion. Nice to have.
   *
   * @return \Drupal\user\UserInterface|null
   *   Returns the current user.
   */
  public static function getCurrentUser() : ?UserInterface {
    static $current_user;
    $current_user ??= \Drupal::currentUser();
    return User::load($current_user->id());
  }

  /**
   * Utility function to get the current language.
   *
   * @return \Drupal\Core\Language\LanguageInterface
   *   Returns the current language.
   */
  public static function getCurrentLanguage() : LanguageInterface {
    static $language_manager;
    $language_manager ??= \Drupal::languageManager();
    return $language_manager->getCurrentLanguage();
  }

  /**
   * Utility function to get the current language ID.
   *
   * @return string
   *   Returns the ID of the current language.
   */
  public static function getCurrentLanguageId() : string {
    return self::getCurrentLanguage()->getId();
  }

  /**
   * Shortcut to get an entity's translation for context.
   *
   * @param ContentEntityInterface $entity
   *   Entity to get translation for.
   *
   * @return ContentEntityInterface
   *   The translated entity.
   */
  public static function getTranslationFromContext(ContentEntityInterface $entity) : ContentEntityInterface {
    static $entity_repository;
    $entity_repository ??= \Drupal::service('entity.repository');
    return $entity_repository->getTranslationFromContext($entity);
  }

  /**
   * Returns the render array of the block with the given machine name.
   *
   * The block MUST be placed within the theme's block layout to be found here.
   * Even disabled blocks can be obtained in this way, as long as they are
   * placed in the active theme's block layout configuration page.
   *
   * @param string $machineName
   *   Machine name of the block to obtain.
   *
   * @return array|null
   *   The render array of the block. NULL if the block could not be found.
   */
  public static function getBlock(string $machineName) : ?array {
    static $entity_type_manager;
    $entity_type_manager ??= \Drupal::entityTypeManager();
    $block = Block::load($machineName);
    if ($block) {
      return $entity_type_manager->getViewBuilder('block')->view($block);
    }
    return NULL;
  }

  /**
   * Build the render array of a given menu via machine name.
   *
   * @param string $machineName
   *   Machine name of the menu to build.
   * @param array $manipulators
   *   Manipulators to use.
   *
   * @return array|null
   *   The render array of the menu.
   *
   * @see https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Menu!menu.api.php/group/menu/11.x
   */
  public static function getMenu(string $machineName, array $manipulators = []) : ?array {
    static $menu_tree;
    $menu_tree ??= \Drupal::menuTree();
    $menu_name = $machineName;

    // Build the typical default set of menu tree parameters.
    $parameters = $menu_tree
      ->getCurrentRouteMenuTreeParameters($menu_name);

    // Load the tree based on this set of parameters.
    $tree = $menu_tree
      ->load($menu_name, $parameters);

    // Transform the tree using the manipulators you want.
    $manipulators = array_merge([
      // Only show links that are accessible for the current user.
      [
        'callable' => 'menu.default_tree_manipulators:checkAccess',
      ],
      // Use the default sorting of menu links.
      [
        'callable' => 'menu.default_tree_manipulators:generateIndexAndSort',
      ],
    ], $manipulators);

    $tree = $menu_tree
      ->transform($tree, $manipulators);

    // Finally, build a renderable array from the transformed tree.
    return $menu_tree->build($tree);
  }

  /**
   * Build the render array of a given form via class name.
   *
   * @param string $class
   *   The full namespace of the class to use to build the form.
   *
   * @return array|null
   *   The render array of the built form if successful.
   */
  public static function getForm(string $class) : ?array {
    static $form_builder;
    $form_builder ??= \Drupal::formBuilder();
    return $form_builder->getForm($class);
  }

  /**
   * Get an image source, generated in a specified image style.
   *
   * @param string $uri
   *   URI of the original image.
   * @param string $style
   *   Machine name of the image style to use.
   *
   * @return string|null
   *   The path to the image generated in the appropriate style.
   *   Returns the original image otherwise.
   */
  public static function getImageInStyle(string $uri, string $style) : ?string {
    static $messenger;
    $messenger ??= \Drupal::messenger();

    // Get image style storage.
    $imageStyle = ImageStyle::load($style);

    if ($imageStyle === NULL) {
      $messenger->addError("The image style $style doesn't exist.");
    }
    else {
      return $imageStyle->buildUrl($uri);
    }

    return Url::fromUri($uri)->toString();
  }

  /**
   * Get responsive image info for an image in a given responsive image style.
   *
   * @param string $uri
   *   Original image URI.
   * @param string $style
   *   Responsive image style machine name.
   *
   * @return array
   *   Array of responsive image information. Returns an empty array otherwise.
   */
  public static function getResponsiveImageInfo(string $uri, string $style) : array {
    static $messenger, $breakpoint_manager;
    $messenger ??= \Drupal::messenger();
    $breakpoint_manager ??= \Drupal::service('breakpoint.manager');

    // Get image style storage.
    $responsiveImageStyle = ResponsiveImageStyle::load($style);

    if ($responsiveImageStyle === NULL) {
      $messenger->addError("The responsive image style $style doesn't exist.");
    }
    else {
      $data = [];
      $data['sources'] = [];
      $breakpoints = array_reverse($breakpoint_manager->getBreakpointsByGroup($responsiveImageStyle->getBreakpointGroup()));
      foreach ($responsiveImageStyle->getImageStyleMappings() as $mapping) {
        $breakpointId = $mapping['breakpoint_id'];
        /** @var \Drupal\breakpoint\Breakpoint $breakpoint */
        $breakpoint = $breakpoints[$breakpointId];
        $multiplier = $mapping['multiplier'];
        switch ($mapping['image_mapping_type']) {
          case 'image_style':
            $data['breakpoints'][$breakpointId . '.' . $multiplier]['type'] = 'style';
            $data['breakpoints'][$breakpointId . '.' . $multiplier]['srcset'] = self::getImageInStyle($uri, $mapping['image_mapping']);
            $data['breakpoints'][$breakpointId . '.' . $multiplier]['media'] = $breakpoint->getMediaQuery();
            $data['sources'][] = [
              'media' => $breakpoint->getMediaQuery(),
              'srcset' => self::getImageInStyle($uri, $mapping['image_mapping']),
            ];
            break;

          case 'sizes':
            $data['breakpoints'][$breakpointId . '.' . $multiplier]['type'] = 'sizes';
            $data['breakpoints'][$breakpointId . '.' . $multiplier]['sizes'] = $mapping['image_mapping']['sizes'];
            $data['breakpoints'][$breakpointId . '.' . $multiplier]['sizes'] = str_replace("\r\n", ', ', $data['breakpoints'][$breakpointId . '.' . $multiplier]['sizes']);
            $data['breakpoints'][$breakpointId . '.' . $multiplier]['srcset'] = [];
            foreach ($mapping['image_mapping']['sizes_image_styles'] as $style) {
              $imageSrc = self::getImageInStyle($uri, $style);
              $styleConfig = (ImageStyle::load($style)->getEffects()->getConfiguration());

              foreach ($styleConfig as $config) {
                if (isset($config['data'])) {
                  if (isset($config['data']['width'])) {
                    $imageStyleWidth = $config['data']['width'];
                  }
                }
              }
              $data['breakpoints'][$breakpointId . '.' . $multiplier]['srcset'][] = $imageSrc . ' ' . $imageStyleWidth . 'w';
            }
            $data['breakpoints'][$breakpointId . '.' . $multiplier]['srcset'] = implode(', ', $data['breakpoints'][$breakpointId . '.' . $multiplier]['srcset']);
            break;
        }
      }
      return $data;
    }

    return [];
  }

}
