# Aide

The Aide module provides a simple utility class called `Aide` containing a
collection of utility functions that cover common Drupal coding tasks.

- [Functions](#functions)
- [Roadmap](#roadmap)

## Functions

* `::getCurrentPath()` - Get the current path.
* `::getRequestUri()` - Get the request URI.
* `::getCurrentRouteName()` - Get the name of the current route.
* `::getCurrentNode()` - Extract node from the current route, if any.
* `::getCurrentUser()` - Get the current user.
* `::getCurrentLanguage()` - Get the current language.
* `::getCurrentLanguageId()` - Get the current language ID.
* `::getBlock(machineName)` - Returns the render array of the block with a given machine name.
* `::getMenu(machineName)` - Build the render array of a given menu via machine name.
* `::getImageInStyle(uri, style)` - Get path to an image file, generated in a specified image style.
* `::getResponsiveImageInfo(uri, style)` - Get responsive image info for an image in a given **responsive** image style.

## Roadmap

* Document readme.
* Add additional utility functions.
* Work out solution fir more backend oriented usage.
  * Dependency Injection + Service?
